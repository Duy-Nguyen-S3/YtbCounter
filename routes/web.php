<?php
Route::get('/', 'Controller@homepage')->name('home');
Route::post('login', 'Auth\LoginController@login')->name('login');

Route::group(['middleware' => 'Login'], function () {
    Route::get('dashboard', 'Admin\AdminController@dashboard')->name('dashboard');
    Route::get('logout', 'Auth\LoginController@logout')->name('logout');
    Route::get('manage', 'Admin\AdminController@manageList')->name('manage');
    Route::get('detail/{id}', 'Admin\AdminController@accessDetailWorkStation')->name('detail');
    Route::get('time', 'Admin\AdminController@changeTime')->name('time');
    Route::get('youtube/list', 'Admin\AdminController@youtubeList')->name('youtubeList');
    Route::post('addlink', 'Admin\AdminController@addLink')->name('addlink');
    Route::post('changetime', 'Admin\AdminController@changeNewTime')->name('changetime');
});

