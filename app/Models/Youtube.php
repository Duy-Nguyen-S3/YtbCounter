<?php

namespace App\Models;

use Session;
use DB;

class Youtube
{
    public static function getList() {
        $res = DB::table('youtube_link')->get();
        return $res;
    }

    public static function getConfigTime() {
        $res = DB::table('config_time')->first();
        return $res->time;
    }

    public static function updateTime($time) {
        return DB::table('config_time')
            ->where('id', 0)
            ->update(['time' => $time]);
    }

    public static function addLink($name, $link) {
        return DB::table('youtube_link')->insert( 
            ['tenvideo' => $name,
             'url' => $link,
             'ngaytao' => time()] );
    }
}
