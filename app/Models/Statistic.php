<?php

namespace App\Models;

use Session;
use DB;

class Statistic
{
    public static function checkout($id, $in, $out) {
        return DB::table('statistic')->insert(
            ['user_id' => $id,
            'giovao' => $in,
            'giora' => $out]
        );
    }
}