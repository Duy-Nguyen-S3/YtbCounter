<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Account;
use App\Models\Youtube;
use Illuminate\Http\Request;

class AdminController extends Controller
{
    public function dashboard(Request $request) {
        return view('admin.main')->with('message', '');
    }

    public function addLink(Request $request) {
        Youtube::addLink($request->name, $request->link);
        return redirect()->back();
    }

    public function manageList() {
        $data = array();
        $res = Account::getWorkStation();
        foreach($res as $item) {
            array_push(
                $data,
                array(
                    'id' => $item->id,
                    'name' => $item->hoten
                )
            );
        }
        return view('admin.manage')->with('data', $data);
    }

    public function accessDetailWorkStation($id) {
        $data = Account::getDetail($id);
        $data->sum = 100;
        return view('admin.detail')->with('data', $data);
    }

    public function changeTime() {
        $data = Youtube::getConfigTime();
        return view('admin.configtime')->with('data', $data);
    }

    public function youtubeList() {
        $data = Youtube::getList();
        return view('admin.youtube_list')->with('data', $data);
    }

    public function changeNewTime(Request $request) {
        $data = $request->time;
        if(is_numeric($data)) {
            Youtube::updateTime($data);
        }
        return redirect()->back();
    }
}
