<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Youtube;
use App\Models\Statistic;
use Illuminate\Http\Request;

class ApiController extends Controller
{
    public function getListURL() {
        $data = Youtube::getList();
        $res = $data;
        $res['config_time'] = Youtube::getConfigTime();
        return json_encode($res, JSON_FORCE_OBJECT);
    }

    public function checkoutClient($id, $in, $out, $key) {
        $dt = (string)$in . (string)$out;
        $__key = '01636414198' . (string)$dt;
        $__key = md5($__key);
        if($__key == $key) {
            Statistic::checkout($id, $in, $out);
            return 'true';
        }
        return 'false';
    }
}
