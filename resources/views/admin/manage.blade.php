@extends('admin.layout')

@section('content')
<div id="page-wrapper" style="min-height: 290px;">
        <div class="graphs">
           <div class="widget_head">Danh sách phòng máy</div>
           @foreach($data as $item)
           <div class="widget_4">
              <div class="col-md-4 widget_1_box1">
                 <div class="coffee">
                    <div class="coffee-top">
                       <a href={{ route('detail', ['id' => $item['id']]) }} target="_blank">
                          <div class="doe">
                             <h6>Lorem Ipusm</h6>
                          <p>Phòng máy {{ $item['id'] }}</p>
                          </div>
                          <i></i>
                       </a>
                    </div>
                    <div class="follow">
                       <div class="col-xs-4 two">
                          <p><b>Họ tên</b></p>
                       </div>
                       <div class="col-xs-4 two">
                          <p><i>{{ $item['name'] }}</i></p>
                          <span>__</span>
                       </div>
                       <div class="clearfix"> </div>
                    </div>
                 </div>
              </div>
@endforeach
            <div class="clearfix"> </div>
           </div>
        </div>
     </div>
@endsection
