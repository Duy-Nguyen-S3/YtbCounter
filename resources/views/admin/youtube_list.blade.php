@extends('admin.layout')

@section('content')
<div id="page-wrapper" style="min-height: 290px;">
    <div class="col-md-12 graphs">
       <div class="xs">
          <h3></h3>
          <div class="panel panel-warning" data-widget="{&quot;draggable&quot;: &quot;false&quot;}" data-widget-static="">
             <div class="panel-heading">
                <h2>Danh sách link Youtube</h2>
                <div class="panel-ctrls" data-actions-container="" data-action-collapse="{&quot;target&quot;: &quot;.panel-body&quot;}"><span class="button-icon has-bg"><i class="ti ti-angle-down"></i></span></div>
             </div>
             <div class="panel-body no-padding" style="display: block;">
                <table class="table table-striped">
                   <thead>
                      <tr class="warning">
                         <th>ID</th>
                         <th>Tên video</th>
                         <th>URL</th>
                         <th>Xóa</th>
                      </tr>
                   </thead>
                   <tbody>
@foreach($data as $item)
                      <tr>
                         <td>{{ $item->id }}</td>
                         <td>{{ $item->tenvideo }}</td>
                         <td>{{ $item->url }}</td>
                         <td><a href="#" title="" class="btn btn-default" data-toggle="dropdown" aria-expanded="false">
			                X
			            <div class="ripple-wrapper"></div></a></td>
                      </tr>
@endforeach
                   </tbody>
                </table>
             </div>
          </div>
          </div>
       </div>
    </div>
 </div>
@endsection
