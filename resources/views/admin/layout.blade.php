<!DOCTYPE HTML>
<html>
   <head>
      <title>NAD</title>
      <script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
      <link href="{{ asset('public/css/bootstrap.min.css') }}" rel='stylesheet' type='text/css' />
      <link href="{{ asset('public/css/style.css') }}" rel='stylesheet' type='text/css' />
      <link href="{{ asset('public/css/font-awesome.css') }}" rel="stylesheet">
      <script src="{{ asset('public/js/jquery.min.js') }}"></script>
      <script src="{{ asset('public/js/bootstrap.min.js') }}"></script>
   </head>
   <body>
      <div id="wrapper">
         <!-- Navigation -->
         <nav class="top1 navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
            <!-- /.navbar-header -->
            <ul class="nav navbar-nav navbar-right">
               <li class="dropdown">
                <a href={{ route('logout') }}>Thoát</a>
               </li>
            </ul>
            <div class="navbar-default sidebar" role="navigation">
               <div class="sidebar-nav navbar-collapse">
                  <ul class="nav" id="side-menu">
                     <li>
                        <a href={{ route('dashboard') }}>
                          <i class="fa fa-dashboard fa-fw nav_icon"></i>Thêm link Youtube</a>
                     </li>
                  </ul>
                  <ul class="nav" id="side-menu">
                      <li>
                         <a href={{ route('manage') }}>
                          <i class="fa fa-dashboard fa-fw nav_icon"></i>Quản lý phòng máy</a>
                      </li>
                   </ul>
                   <ul class="nav" id="side-menu">
                    <li>
                       <a href={{ route('time') }}>
                        <i class="fa fa-dashboard fa-fw nav_icon"></i>Cấu hình thời gian</a>
                    </li>
                 </ul>
                 <ul class="nav" id="side-menu">
                    <li>
                       <a href={{ route('youtubeList') }}>
                        <i class="fa fa-dashboard fa-fw nav_icon"></i>Danh sách link</a>
                    </li>
                 </ul>
               </div>
               <!-- /.sidebar-collapse -->
            </div>
            <!-- /.navbar-static-side -->
         </nav>
        @yield('content')
    <div class="copy_layout">
        <p>Design by <a href="#" target="_blank">NAD Company</a> </p>
    </div>
    </div>
        </div>
         <!-- /#page-wrapper -->
      </div>
      <!-- /#wrapper -->
      <!-- Nav CSS -->
      <link href="{{ asset('public/css/custom.css') }}" rel="stylesheet">
      <!-- Metis Menu Plugin JavaScript -->
      <script src="{{ asset('public/js/metisMenu.min.js') }}"></script>
      <script src="{{ asset('public/js/custom.js') }}"></script>
   </body>
</html>