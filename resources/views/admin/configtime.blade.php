@extends('admin.layout')

@section('content')
<div id="page-wrapper">
    <div class="graphs">
    <div class="xs">
       <h3>Cấu hình thời gian</h3>
       <div class="tab-content">
          <div class="tab-pane active" id="horizontal-form">
            <form class="form-horizontal" action="{{ route('changetime') }}" method="POST">
                {{ csrf_field() }}
                <div class="form-group">
                    <label for="focusedinput" class="col-sm-2 control-label">Thời gian (phút)</label>
                    <div class="col-sm-8">
                       <input type="text" class="form-control1" id="time" name="time" value={{ $data }} style="width: 10%;">
                    </div>
                 </div>
             <div class="panel-footer">
                <div class="row">
                   <div class="col-sm-8 col-sm-offset-2">
                      <button class="btn-success btn">Đổi</button>
                   </div>
                </div>
             </div>
          </form>
       </div>
    </div>
@endsection
