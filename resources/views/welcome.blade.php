<head>
    <title>Đăng nhập</title>
    <script>
        addEventListener("load", function () {
            setTimeout(hideURLbar, 0);
        }, false);

        function hideURLbar() {
            window.scrollTo(0, 1);
        }
    </script>
    <link rel="stylesheet" href="{{ asset('public/css/_style.css') }}" type="text/css" media="all">
    <link href="//fonts.googleapis.com/css?family=Mukta+Mahee:200,300,400,500,600,700,800" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('public/css/font-awesome.css') }}" type="text/css" media="all">
</head>

<body style='background: url({{ asset('public/img/banner.jpg') }}) no-repeat center;'>
    <h1 class="title-agile text-center">Đăng nhập vào trang quản trị</h1>
    <div class="content-w3ls">
        <div class="agileits-grid">
            <div class="content-top-agile">
                <h2>Đăng nhập</h2>
            </div>
            <div class="content-bottom">
                <form role="form" action={{ route('login') }} method="POST">
                    {{ csrf_field() }}
                    <div class="field_w3ls">
                        <div class="field-group">
                            <input name="username" id="username" type="text" value="" required>
                        </div>
                        <div class="field-group">
                            <input id="password" type="password" class="form-control" name="password" value="">
                            <span toggle="#password" class="fa fa-fw fa-eye field-icon toggle-password"></span>
                        </div>
                    </div>
                    <div class="wthree-field">
                        <input id="saveForm" name="saveForm" type="submit" value="Login" />
                    </div>
                    {{$message}}
                </form>
            </div>
            <!-- //content bottom -->
        </div>
    </div>
    <!--//copyright-->
    <script src="{{ asset('public/js/jquery-2.2.3.min.js') }}"></script>
    <!-- script for show password -->
    <script>
        $(".toggle-password").click(function () {

            $(this).toggleClass("fa-eye fa-eye-slash");
            var input = $($(this).attr("toggle"));
            if (input.attr("type") == "password") {
                input.attr("type", "text");
            } else {
                input.attr("type", "password");
            }
        });
    </script>
    <!-- test jenkin 2222333df -->

</body>
<!-- //Body -->

</html>